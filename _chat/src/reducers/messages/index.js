import {
  FETCH_MESSAGES,
  WEBSOCKET_RECEIVE_MESSAGE,
} from '../../actions/messages/types';

const initialState = [];

export default (state = initialState, action) => {
  const {
    type,
    success,
  } = action;

  switch (type) {
    case FETCH_MESSAGES:
    case WEBSOCKET_RECEIVE_MESSAGE: {
      if (success) {
        const {
          data: {
            messages,
          },
        } = action;

        return [
          ...messages,
        ];
      }

      return state;
    }

    default:
      return state;
  }
};
