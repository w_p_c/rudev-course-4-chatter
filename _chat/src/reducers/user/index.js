import {
  LOGIN,
  CHECK_AUTH,
} from '../../actions/authentication/types';

const initialState = null;
export default (state = initialState, action) => {
  const {
    type,
    success,
  } = action;

  switch (type) {
    case LOGIN: {
      if (success) {
        const {
          data: {
            user,
          },
        } = action;

        return {
          ...state,
          ...user,
        };
      }

      return state;
    }

    case CHECK_AUTH: {
      if (success) {
        const {
          data: {
            user,
          },
        } = action;

        return {
          ...state,
          ...user,
        };
      }

      return state;
    }

    default:
      return state;
  }
};
