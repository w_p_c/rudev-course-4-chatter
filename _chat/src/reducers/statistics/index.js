import {
  FETCH_STATISTICS,
} from '../../actions/statistics/types';

const initialState = {};

export default (state = initialState, action) => {
  const {
    type,
    success,
  } = action;

  switch (type) {
    case FETCH_STATISTICS: {
      if (success) {
        const {
          data: {
            messagesByUserAmount,
          },
        } = action;

        return {
          ...state,
          messagesByUserAmount,
        };
      }

      return state;
    }

    default:
      return state;
  }
};
