import {
  CHECK_AUTH,
} from '../../actions/authentication/types';

const initialState = {
  checked: false,
};

export default (state = initialState, action) => {
  const {
    type,
  } = action;

  switch (type) {
    case CHECK_AUTH:
      return {
        ...state,
        checked: true,
      };

    default:
      return state;
  }
};
