import {
  SHOW_SNACK,
  HIDE_SNACK,
} from '../../actions/snackbar/types';

const initialState = {};

export default (state = initialState, action) => {
  const {
    type,
  } = action;

  switch (type) {
    case SHOW_SNACK: {
      const {
        data: {
          name,
          text,
          variant,
          autoHideDuration,
        },
      } = action;

      return {
        ...state,
        [name]: {
          text,
          variant,
          autoHideDuration,
          show: true,
        },
      };
    }

    case HIDE_SNACK: {
      const {
        data: {
          name,
        },
      } = action;

      return {
        ...state,
        [name]: {
          show: false,
        },
      };
    }

    default:
      return state;
  }
};
