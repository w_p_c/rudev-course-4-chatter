import { createStore, applyMiddleware, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import messages from './messages';
import user from './user';
import authentication from './authentication';
import snackbar from './snackbar';
import statistics from './statistics';

const rootReducer = combineReducers({
  messages,
  user,
  authentication,
  snackbar,
  statistics,
});

const store = createStore(
  rootReducer,
  composeWithDevTools(
    applyMiddleware(thunk),
  ),
);

export default store;
