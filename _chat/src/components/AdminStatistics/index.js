import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import style from './style';
import fetchStatistics from '../../actions/statistics';
import { AppBar, Toolbar, IconButton, Icon, Typography } from '@material-ui/core';

const AdminStatistics = class extends React.Component {
  static propTypes = {
    statistics: PropTypes.arrayOf(PropTypes.object),
    user: PropTypes.shape({
      _id: PropTypes.string,
      email: PropTypes.string,
      isAdmin: PropTypes.bool,
    }),
    classes: PropTypes.any,
  }

  componentDidMount() {
    const {
      props,
    } = this;

    props.fetchStatistics();
  }

  render() {
    const {
      props: {
        statistics: {
          messagesByUserAmount,
        },
        user,
        classes,
      },
    } = this;

    if (!user) {
      return (
        <Redirect to="/authentication" />
      );
    }

    if (user && !user.isAdmin) {
      return (
        <Redirect to="/" />
      );
    }

    return (
      <div
        className={classes.root}
      >
        <AppBar position="static">
          <Toolbar>
            <IconButton
              onClick={() => window.history.back()}
              color="inherit"
            >
              <Icon>arrow_back</Icon>
            </IconButton>
            <Typography
              variant="h6"
              color="inherit"
            >
              Admin Statistics
          </Typography>
          </Toolbar>
        </AppBar>
        <Paper
          className={classes.paper}
        >
          {messagesByUserAmount && (
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>
                    User
                  </TableCell>
                  <TableCell
                    align="right"
                  >
                    Messages amount
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {
                  messagesByUserAmount.map(({
                    _id,
                    messagesAmount,
                  }) => (
                      <TableRow key={_id}>
                        <TableCell
                          component="th"
                          scope="row"
                        >
                          {_id}
                        </TableCell>
                        <TableCell
                          align="right"
                        >
                          {messagesAmount}
                        </TableCell>
                      </TableRow>
                    ))}
              </TableBody>
            </Table>
          )}
        </Paper>
      </div>
    );
  }
};

export default compose(
  withStyles(style),
  connect(
    ({
      statistics,
      user,
    }) => ({
      statistics,
      user,
    }),
    {
      fetchStatistics,
    },
  ),
)(AdminStatistics);
