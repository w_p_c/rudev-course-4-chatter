export default theme => {
  const {
    spacing: {
      unit,
    },
  } = theme;

  return {
    root: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'start',
      flexFlow: 'column nowrap',
      width: '100vw',
      height: '100vh',
    },
    paper: {
      maxWidth: unit * 50,
      width: `calc(100vw - ${unit * 3}px)`,
      margin: unit * 3,
    },
  };
};
