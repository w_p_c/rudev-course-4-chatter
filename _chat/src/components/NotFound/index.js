import React from 'react';
import PropTypes from 'prop-types';

import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';

import style from './style';

const NotFound = props => {
  const {
    classes,
  } = props;

  return (
    <div
      className={classes.main}
    >
      <Typography
        variant="h1"
        align="center"
      >
        404 | Page not found
      </Typography>
    </div>
  );
};

NotFound.propTypes = {
  classes: PropTypes.any,
};

export default withStyles(
  style,
)(NotFound);
