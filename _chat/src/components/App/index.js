import React from 'react';
import { Provider } from 'react-redux';
import {
  BrowserRouter,
  Route,
  Switch,
} from 'react-router-dom';

import CSSBaseline from '@material-ui/core/CssBaseline';

import store from '../../reducers';

import Chat from '../Chat';
import Authentication from '../Authentication';
import NotFound from '../NotFound';
import CheckAuth from '../CheckAuth';
import AdminStatistics from '../AdminStatistics';

const App = () => (
  <Provider store={store}>
    <CheckAuth>
      <BrowserRouter>
        <div
          className="app"
          style={{
            height: '100%'
          }}
        >
          <CSSBaseline />
          <Switch>
            <Route
              exact
              path="/"
              component={Chat}
            />
            <Route
              exact
              path="/authentication"
              component={Authentication}
            />
            <Route
              exact
              path="/statistics"
              component={AdminStatistics}
            />
            <Route
              component={NotFound}
            />
          </Switch>
        </div>
      </BrowserRouter>
    </CheckAuth>
  </Provider>
);

export default App;
