import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { compose } from 'redux';
import PropTypes from 'prop-types';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import MaterialLink from '@material-ui/core/Link';
import withStyles from '@material-ui/core/styles/withStyles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import ErrorIcon from '@material-ui/icons/Error';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';

import register from '../../actions/authentication/register';
import hideSnack from '../..//actions/snackbar/hideSnack';
import showSnack from '../..//actions/snackbar/showSnack';

import style from './style';

const variantIcon = {
  success: CheckCircleIcon,
  error: ErrorIcon,
};

const RegisterForm = class extends React.Component {
  static propTypes = {
    register: PropTypes.func,
    hideSnack: PropTypes.func,
    showSnack: PropTypes.func,
    location: PropTypes.any,
    classes: PropTypes.any,
    snackbar: PropTypes.object,
  }

  state = {
    isAdmin: false,
  }

  handleSnackClose = () => {
    const {
      props,
    } = this;

    props.hideSnack('register-form');
  }

  handleCheckboxChange = event => {
    const {
      target: {
        checked,
        value,
      },
    } = event;

    this.setState({
      [value]: checked,
    });
  }

  handleTextInputChange = event => {
    const {
      target: {
        name,
        value,
      },
    } = event;

    this.setState({
      [name]: value,
    });
  }

  handleSubmitForm = event => {
    const {
      props,
      state,
      state: {
        password,
      },
    } = this;

    event.preventDefault();

    if (password !== state['repeat-password']) {
      props.showSnack({
        name: 'register-form',
        text: 'Passwords do not match',
        variant: 'error',
        autoHideDuration: 5000,
      });
      return;
    }

    props.register({
      ...this.state,
    });
  }

  renderSwitchTypeLink = props => {
    const {
      location: {
        pathname,
      },
    } = this.props;

    return (
      <Link
        to={{
          pathname,
          search: '?type=login',
        }}
        {...props}
      />
    );
  }

  render() {
    const {
      props: {
        classes,
        snackbar,
      },
    } = this;

    const snackSettings = snackbar['register-form'];

    const SnackIcon = variantIcon[snackSettings && snackSettings.variant];

    return (
      <div
        className={classes.main}
      >
        <Paper
          className={classes.paper}
        >
          <Avatar
            className={classes.avatar}
          >
            <LockOutlinedIcon />
          </Avatar>
          <Typography
            component="h1"
            variant="h5"
          >
            Register
          </Typography>
          <form
            className={classes.form}
            onSubmit={this.handleSubmitForm}
          >
            <FormControl
              required
              fullWidth
              margin="normal"
            >
              <InputLabel>
                Username
              </InputLabel>
              <Input
                name="login"
                type="text"
                autoFocus
                onChange={this.handleTextInputChange}
              />
            </FormControl>
            <FormControl
              required
              fullWidth
              margin="normal"
            >
              <InputLabel>
                Email Address
              </InputLabel>
              <Input
                name="email"
                type="email"
                autoComplete="email"
                onChange={this.handleTextInputChange}
              />
            </FormControl>
            <FormControl
              required
              fullWidth
              margin="normal"
            >
              <InputLabel>
                Password
              </InputLabel>
              <Input
                name="password"
                type="password"
                onChange={this.handleTextInputChange}
              />
            </FormControl>
            <FormControl
              required
              fullWidth
              margin="normal"
            >
              <InputLabel>
                Repeat Password
              </InputLabel>
              <Input
                name="repeat-password"
                type="password"
                onChange={this.handleTextInputChange}
              />
            </FormControl>
            <FormControlLabel
              control={(
                <Checkbox
                  value="isAdmin"
                  color="primary"
                  onChange={this.handleCheckboxChange}
                />
              )}
              label="Admin user"
            />
            <Button
              className={classes.submit}
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
            >
              Register
            </Button>
            <Typography
              component="p"
              variant="subtitle1"
              align="center"
            >
              Already have an account?
              <MaterialLink
                component={this.renderSwitchTypeLink}
                color="primary"
                variant="subtitle1"
              >
                {' Log In!'}
              </MaterialLink>
            </Typography>
          </form>
        </Paper>
        {snackSettings && snackSettings.show && (
          <Snackbar
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left',
            }}
            open={snackSettings && snackSettings.show}
            autoHideDuration={snackSettings && snackSettings.autoHideDuration}
            onClose={this.handleSnackClose}
          >
            <SnackbarContent
              className={classNames(
                classes[snackSettings && snackSettings.variant],
                classes.snackMargin,
              )}
              aria-describedby="client-snackbar"
              message={(
                <span
                  id="client-snackbar"
                  className={classes.message}

                >
                  <SnackIcon
                    className={classNames(classes.snackIcon, classes.snackIconVariant)}
                  />
                  {snackSettings && snackSettings.text}
                </span>
              )}
              action={[
                <IconButton
                  key="close"
                  aria-label="Close"
                  color="inherit"
                  className={classes.snackClose}
                  onClick={this.handleSnackClose}
                >
                  <CloseIcon
                    className={classes.snackIcon}
                  />
                </IconButton>,
              ]}
            />
          </Snackbar>
        )}
      </div>
    );
  }
};

export default compose(
  withStyles(style),
  connect(
    ({
      snackbar,
    }) => ({
      snackbar,
    }),
    {
      register,
      hideSnack,
      showSnack,
    },
  ),
)(RegisterForm);
