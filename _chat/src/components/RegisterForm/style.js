import green from '@material-ui/core/colors/green';

export default theme => {
  const {
    spacing: {
      unit,
    },
    breakpoints: {
      up,
    },
    palette,
  } = theme;

  return ({
    snackClose: {
      padding: theme.spacing.unit / 2,
    },
    snackMargin: {
      margin: unit,
    },
    snackIcon: {
      fontSize: 20,
    },
    snackIconVariant: {
      opacity: 0.9,
      marginRight: theme.spacing.unit,
    },
    message: {
      display: 'flex',
      alignItems: 'center',
    },
    error: {
      backgroundColor: palette.error.dark,
    },
    success: {
      backgroundColor: green[600],
    },
    main: {
      paddingTop: unit * 8,
      width: 'auto',
      display: 'block',
      marginLeft: unit * 3,
      marginRight: unit * 3,
      [up(400 + unit * 3 * 2)]: {
        width: 400,
        marginLeft: 'auto',
        marginRight: 'auto',
      },
    },
    paper: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      padding: `${unit * 2}px ${unit * 3}px ${unit * 3}px`,
    },
    avatar: {
      margin: unit,
      backgroundColor: palette.secondary.main,
    },
    form: {
      width: '100%',
      marginTop: unit,
    },
    submit: {
      marginTop: unit * 3,
      marginBottom: unit * 3,
    },
  });
};
