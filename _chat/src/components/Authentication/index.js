import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import queryString from 'query-string';
import PropTypes from 'prop-types';

import LogInForm from '../LogInForm';
import RegisterForm from '../RegisterForm';

const Authentication = props => {
  const {
    user,
    location,
    location: {
      search,
      pathname,
    },
  } = props;

  if (user) {
    return (
      <Redirect to="/" />
    );
  }

  const searchParsed = queryString.parse(search);

  const {
    type,
  } = searchParsed;

  if (
    !type
    || (
      type !== 'login'
      && type !== 'register'
    )
  ) {
    return (
      <Redirect
        to={{
          pathname,
          search: '?type=login',
        }}
      />
    );
  }

  return (
    <div
      className="authentication"
      style={{
        height: '100%'
      }}
    >
      {type === 'login' && (
        <LogInForm
          location={location}
        />
      )}
      {type === 'register' && (
        <RegisterForm
          location={location}
        />
      )}
    </div>
  );
};

Authentication.propTypes = {
  user: PropTypes.shape({
    _id: PropTypes.string,
    email: PropTypes.string,
    isAdmin: PropTypes.bool,
  }),
  location: PropTypes.any,
};

export default connect(
  ({
    user,
  }) => ({
    user,
  }),
)(Authentication);
