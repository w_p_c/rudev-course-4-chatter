export default theme => {
  const {
    spacing: {
      unit,
    },
  } = theme;

  return ({
    main: {
      display: 'flex',
      flexFlow: 'column nowrap',
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      height: `calc(100% + ${window.detectBrowser.os === "Android OS" ? 8: 0}px)`,
      marginTop: `${window.detectBrowser.os === "Android OS" ? 28 : 0}px`
    },
    paperChat: {
      flexGrow: 1,
      maxWidth: `${100 * unit}px`,
      width: '100vw',
      display: 'flex',
      flexFlow: 'column nowrap',
      justifyContent: 'center',
      alignItems: 'center',
      height: '100%',
    },
    list: {
      flexGrow: 1,
      overflowY: 'auto',
      width: '100%',
    },
    paperSend: {
      maxWidth: `${100 * unit}px`,
      width: '100vw',
      height: `${20 * unit}px`,
      maxHeight: '20vh',
      minHeight: `${20 * unit}px`,
      display: 'flex',
      flexFlow: 'row nowrap',
      justifyContent: 'center',
      alignItems: 'center',
      // backgroundColor: grey[100],
    },
    rightIcon: {
      marginLeft: theme.spacing.unit,
    },
    buttonSend: {
      margin: theme.spacing.unit,
    },
    newMessageText: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
      flexGrow: 1,
    },
    divider: {
      width: '100%',
    },
  });
};
