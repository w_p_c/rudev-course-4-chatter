import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';
import moment from 'moment';
import { compose } from 'redux';
import PropTypes from 'prop-types';

import withStyles from '@material-ui/core/styles/withStyles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Icon from '@material-ui/core/Icon';
import Divider from '@material-ui/core/Divider';

import logout from '../../actions/authentication/logout';
import fetchMessages from '../../actions/messages/fetchMessages';
import sendMessage from '../../actions/messages/sendMessage';

import style from './style';

const Chat = class extends React.Component {
  static propTypes = {
    classes: PropTypes.any,
    user: PropTypes.shape({
      _id: PropTypes.string,
      email: PropTypes.string,
      isAdmin: PropTypes.bool,
    }),
    messages: PropTypes.arrayOf(PropTypes.shape({
      _id: PropTypes.string,
      creatorId: PropTypes.string,
      createdAt: PropTypes.string,
      text: PropTypes.string,
    })),
  }

  state = {
    message: '',
  }

  componentDidMount() {
    const {
      props,
    } = this;

    props.fetchMessages();
  }

  handleClickLogOut = () => {
    const {
      props,
    } = this;

    props.logout();
  }

  handleClickSend = () => {
    const {
      state: {
        message,
      },
      props,
    } = this;

    props.sendMessage({
      text: message,
    });

    this.setState({
      message: '',
    });
  }

  handleTextInputChange = event => {
    const {
      target: {
        name,
        value,
      },
    } = event;

    this.setState({
      [name]: value,
    });
  }

  render() {
    const {
      props: {
        user,
        classes,
        messages,
      },
      state: {
        message,
      },
    } = this;

    if (!user) {
      return (
        <Redirect to="/authentication" />
      );
    }

    const {
      isAdmin,
      _id
    } = user;

    return (
      <div
        className={classes.main}
      >
        <AppBar
          position="static"
        >
          <Toolbar
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
              flexFlow: 'row wrap'
            }}
          >
            <Typography
              variant="h6"
              color="inherit"
            >
              {_id}
            </Typography>
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              {isAdmin && (
                <Link
                  to="/statistics"
                  style={{
                    textDecoration: 'none',
                    color: 'inherit',
                  }}
                >
                  <Button
                    color="inherit"
                  >
                    Admin Statistics
                </Button>
                </Link>
              )}
              <Button
                color="inherit"
                onClick={this.handleClickLogOut}
              >
                Log out
            </Button>
            </div>
          </Toolbar>
        </AppBar>
        <Paper
          className={classes.paperChat}
        >
          <List
            className={classes.list}
          >
            {messages.map(el => {
              const {
                id,
                text,
                creatorId,
                createdAt,
              } = el;

              const selfMessage = creatorId === user._id;

              const messageSide = selfMessage ? 'right' : 'left';
              const creatorIdWritten = selfMessage ? 'You' : creatorId;

              const prettyDate = moment(createdAt).fromNow();

              return (
                <ListItem
                  key={id}
                  divider
                >
                  <ListItemText
                    primary={creatorIdWritten}
                    primaryTypographyProps={{
                      align: messageSide,
                      noWrap: true,
                    }}
                    secondary={(
                      <Fragment>
                        <Typography
                          align={messageSide}
                          noWrap
                        >
                          {text}
                        </Typography>
                        <Typography
                          align={messageSide}
                          noWrap
                        >
                          {prettyDate}
                        </Typography>
                      </Fragment>
                    )}
                  />
                </ListItem>
              );
            })}
          </List>
          <Divider
            className={classes.divider}
          />
          <Paper
            elevation={0}
            className={classes.paperSend}
          >
            <TextField
              name="message"
              label="Message"
              multiline
              rows="4"
              className={classes.newMessageText}
              margin="normal"
              variant="outlined"
              onChange={this.handleTextInputChange}
              value={message}
            />
            <Button
              variant="fab"
              color="primary"
              className={classes.buttonSend}
              onClick={this.handleClickSend}
            >
              <Icon
                className={classes.rightIcon}
              >
                send
              </Icon>
            </Button>
          </Paper>
        </Paper>
      </div>
    );
  }
};

export default compose(
  withStyles(style),
  connect(
    ({
      user,
      messages,
    }) => ({
      user,
      messages,
    }),
    {
      logout,
      fetchMessages,
      sendMessage,
    },
  ),
)(Chat);
