import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import checkAuth from '../../actions/authentication/checkAuth';

const CheckAuth = class extends React.Component {
  static propTypes = {
    authentication: PropTypes.shape({
      checked: PropTypes.bool,
    }),
    children: PropTypes.any,
  }

  componentDidMount() {
    const {
      props,
      props: {
        authentication: {
          checked,
        },
      },
    } = this;

    if (!checked) {
      props.checkAuth();
    }
  }

  render() {
    const {
      props: {
        children,
      },
    } = this;

    return (
      <Fragment>
        {children}
      </Fragment>
    );
  }
};

export default connect(
  ({
    authentication,
  }) => ({
    authentication,
  }),
  {
    checkAuth,
  },
)(CheckAuth);
