import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { compose } from 'redux';
import PropTypes from 'prop-types';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import MaterialLink from '@material-ui/core/Link';
import withStyles from '@material-ui/core/styles/withStyles';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import ErrorIcon from '@material-ui/icons/Error';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';

import login from '../../actions/authentication/login';
import hideSnack from '../..//actions/snackbar/hideSnack';

import style from './style';

const variantIcon = {
  success: CheckCircleIcon,
  error: ErrorIcon,
};

const LoginForm = class extends React.Component {
  static propTypes = {
    hideSnack: PropTypes.func,
    login: PropTypes.func,
    location: PropTypes.any,
    classes: PropTypes.any,
    snackbar: PropTypes.object,
  }

  handleSnackClose = () => {
    const {
      props,
    } = this;

    props.hideSnack('login-form');
  }

  handleTextInputChange = event => {
    const {
      target: {
        name,
        value,
      },
    } = event;

    this.setState({
      [name]: value,
    });
  }

  handleSubmitForm = event => {
    const {
      props,
    } = this;

    event.preventDefault();

    props.login({
      ...this.state,
    });
  }

  renderSwitchTypeLink = props => {
    const {
      location: {
        pathname,
      },
    } = this.props;

    return (
      <Link
        to={{
          pathname,
          search: '?type=register',
        }}
        {...props}
      />
    );
  }

  render() {
    const {
      props: {
        classes,
        snackbar,
      },
    } = this;

    const snackSettings = snackbar['login-form'];

    const SnackIcon = variantIcon[snackSettings && snackSettings.variant];

    return (
      <div
        className={classes.main}
      >
        <Paper
          className={classes.paper}
        >
          <Avatar
            className={classes.avatar}
          >
            <LockOutlinedIcon />
          </Avatar>
          <Typography
            component="h1"
            variant="h5"
          >
            Log In
          </Typography>
          <form
            className={classes.form}
            onSubmit={this.handleSubmitForm}
          >
            <FormControl
              required
              fullWidth
              margin="normal"
            >
              <InputLabel>
                Email Address
              </InputLabel>
              <Input
                name="email"
                type="email"
                autoComplete="email"
                autoFocus
                onChange={this.handleTextInputChange}
              />
            </FormControl>
            <FormControl
              required
              fullWidth
              margin="normal"
            >
              <InputLabel>
                Password
              </InputLabel>
              <Input
                name="password"
                type="password"
                autoComplete="current-password"
                onChange={this.handleTextInputChange}
              />
            </FormControl>
            <Button
              className={classes.submit}
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
            >
              Log In
            </Button>
            <Typography
              component="p"
              variant="subtitle1"
              align="center"
            >
              Do not have an account?
              <MaterialLink
                component={this.renderSwitchTypeLink}
                color="primary"
                variant="subtitle1"
              >
                {' Register!'}
              </MaterialLink>
            </Typography>
          </form>
        </Paper>
        {snackSettings && snackSettings.show && (
          <Snackbar
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left',
            }}
            open={snackSettings && snackSettings.show}
            autoHideDuration={snackSettings && snackSettings.autoHideDuration}
            onClose={this.handleSnackClose}
          >
            <SnackbarContent
              className={classNames(
                classes[snackSettings && snackSettings.variant],
                classes.snackMargin,
              )}
              aria-describedby="client-snackbar"
              message={(
                <span
                  id="client-snackbar"
                  className={classes.message}

                >
                  <SnackIcon
                    className={classNames(classes.snackIcon, classes.snackIconVariant)}
                  />
                  {snackSettings && snackSettings.text}
                </span>
              )}
              action={[
                <IconButton
                  key="close"
                  aria-label="Close"
                  color="inherit"
                  className={classes.snackClose}
                  onClick={this.handleSnackClose}
                >
                  <CloseIcon
                    className={classes.snackIcon}
                  />
                </IconButton>,
              ]}
            />
          </Snackbar>
        )}
      </div>
    );
  }
};

export default compose(
  withStyles(style),
  connect(
    ({
      snackbar,
    }) => ({
      snackbar,
    }),
    {
      login,
      hideSnack,
    },
  ),
)(LoginForm);
