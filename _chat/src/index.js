import React from 'react';
import ReactDOM from 'react-dom';
import { detect as detectBrowser } from 'detect-browser';

import App from './components/App';
import './index.css';
import config from '../../config';

window.appConfig = config;
window.detectBrowser = detectBrowser();

if (window.detectBrowser.os === "Android OS") {
  document.body.style.height = `calc(${document.documentElement.clientHeight}px - 64px)`;
}

window.dispatchEvent(new Event('resize'));

ReactDOM.render(<App />, document.getElementById('root'));
