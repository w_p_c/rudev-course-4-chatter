import {
  SHOW_SNACK,
} from './types';

export default ({
  name,
  text,
  variant,
  autoHideDuration,
}) => ({
  type: SHOW_SNACK,
  data: {
    name,
    text,
    variant,
    autoHideDuration,
  },
});
