import {
  HIDE_SNACK,
} from './types';

export default name => ({
  type: HIDE_SNACK,
  data: {
    name,
  },
});
