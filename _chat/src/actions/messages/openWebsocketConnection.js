import jsCookie from 'js-cookie';

import {
  WEBSOCKET_CLOSE_CONNECTION,
  WEBSOCKET_OPEN_CONNECTION,
  WEBSOCKET_RECEIVE_ERROR,
  WEBSOCKET_RECEIVE_MESSAGE,
} from './types';

const onOpen = dispatch => () => {
  dispatch({
    type: WEBSOCKET_OPEN_CONNECTION,
    success: true,
  });
};

const onMessage = dispatch => event => {
  const {
    data,
  } = event;

  const messages = JSON.parse(data);

  dispatch({
    type: WEBSOCKET_RECEIVE_MESSAGE,
    success: true,
    data: {
      messages,
    },
  });
};

const onError = dispatch => error => {
  console.error(`ERR: ${error}`);

  dispatch({
    type: WEBSOCKET_RECEIVE_ERROR,
    data: {
      error,
    },
  });
};

const onClose = dispatch => event => {
  let success = null;

  if (event.wasClean) {
    success = true;
  } else {
    success = false;
  }

  dispatch({
    type: WEBSOCKET_CLOSE_CONNECTION,
    success,
  });
};

export default () => dispatch => {
  const {
    dev: {
      server: {
        port
      },
    },
  } = window.appConfig;

  const token = jsCookie.get('chatter_token');
  const rsaPublic = jsCookie.getJSON('chatter_rsaPublic');

  /* eslint-disable no-undef */
  const socket = new WebSocket(`ws://${window.location.hostname}:${port}/socket/messages?token=${encodeURIComponent(token)}&rsaPublic=${encodeURIComponent(rsaPublic)}`);

  socket.onopen = onOpen(dispatch);
  socket.onclose = onClose(dispatch);
  socket.onerror = onError(dispatch);
  socket.onmessage = onMessage(dispatch);
};
