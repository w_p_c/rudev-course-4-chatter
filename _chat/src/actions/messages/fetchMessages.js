import {
  FETCH_MESSAGES,
} from './types';

import callApi from '../../utils/callApi';
import openWebsocketConnection from './openWebsocketConnection';

export default () => dispatch => {
  callApi(
    '/messages/get',
    null,
    true,
  ).then(
    res => {
      const {
        data: {
          success,
          data: {
            messages,
          },
        },
      } = res;

      dispatch({
        type: FETCH_MESSAGES,
        data: {
          messages,
        },
        success,
      });

      dispatch(openWebsocketConnection());
    },
    rej => {
      console.error(rej);

      dispatch({
        type: FETCH_MESSAGES,
        success: false,
      });
    },
  );
};
