import {
  SEND_MESSAGE,
} from './types';

import callApi from '../../utils/callApi';

export default ({ text }) => dispatch => {
  callApi(
    '/messages/send',
    {
      text,
    },
    true,
  ).then(
    res => {
      const {
        data: {
          success,
        },
      } = res;

      dispatch({
        type: SEND_MESSAGE,
        success,
      });
    },
    rej => {
      console.error(rej);

      dispatch({
        type: SEND_MESSAGE,
        success: false,
      });
    },
  );
};
