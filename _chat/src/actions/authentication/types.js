export const LOGIN = 'LOGIN';
export const REGISTER = 'REGISTER';
export const CHECK_AUTH = 'CHECK_AUTH';
export const LOGOUT = 'LOGOUT';
