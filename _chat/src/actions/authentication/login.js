import callApi from '../../utils/callApi';
import { LOGIN } from './types';

import showSnack from '../snackbar/showSnack';

export default ({
  email,
  password,
}) => dispatch => {
  callApi(
    '/authentication/login',
    {
      email,
      password,
    },
  ).then(
    res => {
      const {
        data: {
          data: {
            user,
          },
          success,
        },
      } = res;

      dispatch({
        type: LOGIN,
        data: {
          user,
        },
        success,
      });
    },
    rej => {
      const {
        response: {
          data: {
            description,
          },
        },
      } = rej;

      dispatch(showSnack({
        name: 'login-form',
        text: description,
        variant: 'error',
        autoHideDuration: 5000,
      }));
    },
  );
};
