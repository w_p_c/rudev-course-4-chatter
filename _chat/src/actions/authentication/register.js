import { REGISTER } from './types';

import showSnack from '../snackbar/showSnack';
import callApi from '../../utils/callApi';

export default ({
  email,
  password,
  login,
  isAdmin,
}) => dispatch => {
  callApi(
    '/authentication/register',
    {
      email,
      password,
      login,
      isAdmin,
    },
  ).then(
    res => {
      const {
        data: {
          success,
          description,
        },
      } = res;

      dispatch({
        type: REGISTER,
        success,
      });
      dispatch(showSnack({
        name: 'register-form',
        text: description,
        variant: 'success',
        autoHideDuration: 5000,
      }));
    },
    rej => {
      const {
        response: {
          data: {
            description,
          },
        },
      } = rej;

      dispatch(showSnack({
        name: 'register-form',
        text: description,
        variant: 'error',
        autoHideDuration: 5000,
      }));
    },
  );
};
