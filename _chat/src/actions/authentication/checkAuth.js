import { CHECK_AUTH } from './types';

import callApi from '../../utils/callApi';

export default () => dispatch => {
  callApi(
    '/user/self',
    null,
    true,
  ).then(
    res => {
      const {
        data: {
          data: {
            user,
          },
          success,
        },
      } = res;

      dispatch({
        type: CHECK_AUTH,
        data: {
          user,
        },
        success,
      });
    },
    () => {
      dispatch({
        type: CHECK_AUTH,
        success: false,
      });
    },
  );
};
