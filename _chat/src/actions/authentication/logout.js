import {
  LOGOUT,
} from './types';

import callApi from '../../utils/callApi';

export default () => dispatch => {
  callApi(
    '/authentication/logout',
    null,
    true,
  ).then(
    () => {
      dispatch({
        type: LOGOUT,
        success: true,
      });

      window.location.reload();
    },
    () => {
      dispatch({
        type: LOGOUT,
        success: false,
      });

      window.location.reload();
    },
  );
};
