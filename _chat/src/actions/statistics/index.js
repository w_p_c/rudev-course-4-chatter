import {
  FETCH_STATISTICS,
} from './types';

import callApi from '../../utils/callApi';

export default () => dispatch => {
  callApi(
    '/admin/statistics',
    null,
    true,
  ).then(
    res => {
      const {
        data: {
          success,
          data: {
            messagesByUserAmount,
          },
        },
      } = res;

      dispatch({
        type: FETCH_STATISTICS,
        data: {
          messagesByUserAmount,
        },
        success,
      });
    },
    rej => {
      console.error(rej);

      dispatch({
        type: FETCH_STATISTICS,
        success: false,
      });
    },
  );
};
