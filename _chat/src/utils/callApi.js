import axios from 'axios';
import jsCookie from 'js-cookie';

export default (path, data, withAuth = false) => {
  const {
    dev: {
      server: {
        port,
        host,
      },
    },
  } = window.appConfig;

  let dataSent = data;

  if (withAuth) {
    const token = jsCookie.get('chatter_token');
    const rsaPublic = jsCookie.getJSON('chatter_rsaPublic');

    dataSent = {
      ...dataSent,
      token,
      rsaPublic,
    };
  }

  return axios.post(
    `http://${window.location.hostname}:${port}${path}`,
    dataSent,
  );
};
