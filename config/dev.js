module.exports = {
  mongodb: {
    name: 'chatter',
    port: 27017,
    host: 'localhost',
    dbpath: '/data/mongodb',
    logpath: '/data/mongolog',
  },
  server: {
    port: 49000,
    host: 'localhost',
  },
};
