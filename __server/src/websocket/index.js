const url = require('url');
const querystring = require('querystring');

global.wss = {};

const messages = require('./messages');
const jwt = require('../utils/jwt');

module.exports = (db, dbcol) => (request, socket, head) => {
  /* eslint-disable prefer-destructuring */
  const parsedURL = url.parse(request.url);

  const parsedQuery = querystring.parse(parsedURL.query);

  const {
    token,
    rsaPublic,
  } = parsedQuery;

  jwt.verify(token, rsaPublic)
    .then(
      res => res,
    )
    .then(
      verifiedJWT => {
        const {
          userId,
        } = verifiedJWT;

        return db.collection(dbcol.jwt.name).findOne({
          _id: userId,
        });
      },
    )
    .then(
      mFindJWT => {
        const {
          jwts,
        } = mFindJWT;

        if (jwts && jwts.includes(token)) {
          const prefix = '/socket';
          switch (parsedURL.pathname) {
            case `${prefix}/messages`:
              messages.handleUpgrade(request, socket, head, ws => {
                messages.emit('connection', ws, request);
              });
              break;

            default:
              throw new Error('No websocket exists on requested path.');
          }
        }
      },
    )
    .catch(
      rej => {
        socket.destroy();
        console.log(`Error during websocket handshake: ${rej.message}`);
      },
    );
};
