const ws = require('ws');

const wss = new ws.Server({
  noServer: true,
});

wss.broadcast = data => {
  wss.clients.forEach(client => {
    if (client.readyState === ws.OPEN) {
      client.send(data);
    }
  });
};

wss.on('connection', () => {
  console.log('Websocket connection to "messages" complete.');
});

global.wss.messages = wss;

module.exports = wss;
