module.exports = () => async (ctx, next) => {
  const {
    request: {
      method,
      originalUrl,
    },
  } = ctx;

  console.log(`Incoming req: ${method}: ${originalUrl}`);

  await next();
};
