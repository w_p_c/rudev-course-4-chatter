module.exports = status => async (ctx, next) => {
  ctx.response.status = status;

  await next();
};
