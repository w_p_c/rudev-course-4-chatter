const jwt = require('../utils/jwt');
const {
  ResponseError,
  respondWithError,
} = require('../utils/respondWithError');

const verifyJWT = exceptPaths => async (ctx, next) => {
  try {
    const {
      request: {
        url,
        body,
      },
    } = ctx;

    if (exceptPaths) {
      const exceptPath = exceptPaths.find(el => url.search(el) === 0);

      if (exceptPath) {
        return next();
      }
    }

    if (body) {
      try {
        const {
          token,
          rsaPublic,
        } = body;

        let verified = null;
        try {
          verified = await jwt.verify(token, rsaPublic);
        } catch (err) {
          throw new ResponseError(401, err.toString());
        }

        const {
          userId,
        } = verified;

        const {
          db,
          dbcol,
        } = ctx;

        const findJWT = await db.collection(dbcol.jwt.name).findOne({
          _id: userId,
        });

        if (findJWT && findJWT.jwts && findJWT.jwts.includes(token)) {
          ctx.verifiedJWT = verified;
          return next();
        }

        throw new ResponseError(401, 'Token is invalid or expired');
      } catch (err) {
        switch (ctx.request.method) {
          case 'GET':
            ctx.response.redirect('/authentication');
            break;

          default:
            throw err;
        }
      }
    }
  } catch (err) {
    respondWithError(err, ctx);
  }
};

module.exports = verifyJWT;
