const jwt = require('jsonwebtoken');

const sign = (userId, rsaPrivate) => new Promise(res => {
  const signed = jwt.sign(
    {
      userId,
      signedAt: Date.now(),
    },
    rsaPrivate,
    {
      algorithm: 'RS256',
    },
  );

  res(signed);
});

const verify = (token, rsaPublic) => new Promise((res, rej) => {
  jwt.verify(
    token,
    rsaPublic,
    {
      algorithms: ['RS256'],
    },
    (err, decoded) => {
      if (err) {
        rej(new Error('Token is invalid.'));
      } else {
        res(decoded);
      }
    },
  );
});

module.exports = {
  sign,
  verify,
};
