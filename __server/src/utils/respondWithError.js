class ResponseError {
  constructor(status = 404, message, data) {
    this.status = status;
    this.message = message;
    this.data = data;
  }
}

const respondWithError = (err, ctx) => {
  const {
    constructor: {
      name,
    },
  } = err;

  if (name === ResponseError.name) {
    const {
      status,
      message,
      data,
    } = err;

    ctx.response.status = status;
    ctx.response.body = {
      success: false,
      description: message,
      data,
    };
  } else {
    throw err;
  }
};

module.exports = {
  respondWithError,
  ResponseError,
};
