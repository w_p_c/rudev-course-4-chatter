const Router = require('koa-router');

const verifyJWT = require('../../../middleware/verifyJWT');
const {
  respondWithError,
} = require('../../../utils/respondWithError');

const router = new Router();

router
  .use(verifyJWT())
  .post('/', async ctx => {
    try {
      const {
        request: {
          body: {
            text,
          },
        },
        verifiedJWT: {
          userId,
        },
        db,
        dbcol,
      } = ctx;

      const {
        wss: {
          messages,
        },
      } = global;

      await db.collection(dbcol.message.name).insertOne({
        creatorId: userId,
        text,
        createdAt: new Date(),
      });

      const mFindMessages = await db.collection(dbcol.message.name)
        .find()
        .sort({
          createdAt: 1,
        })
        .toArray();

      messages.broadcast(JSON.stringify(mFindMessages));

      ctx.response.body = {
        success: true,
      };
    } catch (err) {
      respondWithError(err);
    }
  });

module.exports = router;
