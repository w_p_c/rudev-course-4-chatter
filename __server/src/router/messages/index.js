const Router = require('koa-router');

const send = require('./send');
const get = require('./get');

const router = new Router();

router
  .use('/get', get.routes(), get.allowedMethods())
  .use('/send', send.routes(), send.allowedMethods());

module.exports = router;
