const Router = require('koa-router');

const verifyJWT = require('../../../middleware/verifyJWT');
const {
  respondWithError,
} = require('../../../utils/respondWithError');

const router = new Router();

router
  .use(verifyJWT())
  .post('/', async ctx => {
    try {
      const {
        db,
        dbcol,
      } = ctx;

      const mFindMessages = await db.collection(dbcol.message.name)
        .find()
        .sort({
          createdAt: 1,
        })
        .toArray();

      ctx.response.body = {
        success: true,
        data: {
          messages: mFindMessages,
        },
      };
    } catch (err) {
      respondWithError(err);
    }
  });

module.exports = router;
