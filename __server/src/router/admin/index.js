const Router = require('koa-router');

const statistics = require('./statistics/_root');

const router = new Router();

router
  .use('/statistics', statistics.routes(), statistics.allowedMethods());

module.exports = router;
