const Router = require('koa-router');

const verifyJWT = require('../../../../middleware/verifyJWT');
const {
  ResponseError,
  respondWithError,
} = require('../../../../utils/respondWithError');

const router = new Router();

router
  .use(verifyJWT())
  .post('/', async ctx => {
    try {
      const {
        verifiedJWT: {
          userId,
        },
        db,
        dbcol,
      } = ctx;

      const mFindUser = await db.collection(dbcol.user.name).findOne({
        _id: userId,
      });

      const {
        isAdmin,
      } = mFindUser;

      if (!isAdmin) {
        throw new ResponseError(
          404,
          'User does not have permissions.',
        );
      }

      const mAggregateMessages = await db.collection(dbcol.message.name)
        .aggregate(
          [
            {
              $group: {
                _id: '$creatorId',
                messagesAmount: {
                  $sum: 1,
                },
              },
            },
          ],
        )
        .sort({
          _id: 1,
        })
        .toArray();

      ctx.response.body = {
        success: true,
        data: {
          messagesByUserAmount: mAggregateMessages,
        },
      };
    } catch (err) {
      respondWithError(err);
    }
  });

module.exports = router;
