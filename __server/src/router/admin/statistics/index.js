const Router = require('koa-router');

const root = require('./_root');

const router = new Router();

router
  .use(root.routes(), root.allowedMethods());

module.exports = router;
