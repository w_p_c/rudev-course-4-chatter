const Router = require('koa-router');

const root = require('./_root');
const authentication = require('./authentication');
const messages = require('./messages');
const user = require('./user');
const admin = require('./admin');

const router = new Router();

router
  .use('/authentication', authentication.routes(), authentication.allowedMethods())
  .use('/messages', messages.routes(), messages.allowedMethods())
  .use('/user', user.routes(), user.allowedMethods())
  .use('/admin', admin.routes(), admin.allowedMethods())
  .use(root.routes(), root.allowedMethods());

module.exports = router;
