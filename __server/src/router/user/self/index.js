const Router = require('koa-router');

const verifyJWT = require('../../../middleware/verifyJWT');
const {
  respondWithError,
} = require('../../../utils/respondWithError');

const router = new Router();

router
  .use(verifyJWT())
  .post('/', async ctx => {
    try {
      const {
        verifiedJWT: {
          userId,
        },
        db,
        dbcol,
      } = ctx;

      const mFindUser = await db.collection(dbcol.user.name).findOne({
        _id: userId,
      });

      const userSent = {
        ...mFindUser,
      };
      delete userSent.password;

      ctx.response.body = {
        success: true,
        data: {
          user: userSent,
        },
      };
    } catch (err) {
      respondWithError(err);
    }
  });

module.exports = router;
