const Router = require('koa-router');

const self = require('./self');

const router = new Router();

router
  .use('/self', self.routes(), self.allowedMethods());

module.exports = router;
