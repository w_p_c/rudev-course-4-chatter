const Router = require('koa-router');

const {
  respondWithError,
} = require('../../../utils/respondWithError');
const verifyJWT = require('../../../middleware/verifyJWT');

const router = new Router();

router
  .use(verifyJWT())
  .post('/', async ctx => {
    try {
      const {
        request: {
          body: {
            token,
          },
        },
        verifiedJWT: {
          userId,
        },
      } = ctx;

      const {
        db,
        dbcol,
      } = ctx;

      await db.collection(dbcol.jwt.name).updateOne(
        {
          _id: userId,
        },
        {
          $pullAll: {
            jwts: [token],
          },
        },
        {
          upsert: true,
        },
      );

      ctx.cookies.set('chatter_token', '');
      ctx.cookies.set('chatter_rsaPublic', '');
      ctx.response.body = {
        status: true,
        description: `User ${userId} successfully logged out`,
      };
    } catch (err) {
      respondWithError(err, ctx);
    }
  });

module.exports = router;
