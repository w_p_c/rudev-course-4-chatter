const Router = require('koa-router');

const root = require('./_root');
const login = require('./login');
const register = require('./register');
const logout = require('./logout');

const router = new Router();

router
  .use('/login', login.routes(), register.allowedMethods())
  .use('/register', register.routes(), register.allowedMethods())
  .use('/logout', logout.routes(), logout.allowedMethods())
  .use(root.routes(), root.allowedMethods());

module.exports = router;
