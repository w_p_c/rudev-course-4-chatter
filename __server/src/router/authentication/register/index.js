const Router = require('koa-router');
const keypair = require('keypair');

const {
  respondWithError,
  ResponseError,
} = require('../../../utils/respondWithError');

const router = new Router();

router.post('/', async ctx => {
  try {
    const {
      login,
      email,
      password,
      isAdmin,
    } = ctx.request.body;

    const {
      db,
      dbcol,
    } = ctx;

    if (
      !login
      || !email
      || !password
      || (typeof isAdmin !== 'boolean')
    ) {
      throw new ResponseError(
        404,
        'Missing required fields.',
        {
          login,
          email,
          password,
          isAdmin,
        },
      );
    }

    const mFindUserByEmail = await db.collection(dbcol.user.name).findOne({
      email,
    });

    const mFindUserByLogin = await db.collection(dbcol.user.name).findOne({
      _id: login,
    });

    if (mFindUserByEmail || mFindUserByLogin) {
      throw new ResponseError(
        404,
        'User with such email or login already registered',
        {
          email: Boolean(mFindUserByEmail),
          login: Boolean(mFindUserByLogin),
        },
      );
    }

    const rsaKeypair = await new Promise(res => {
      const result = keypair({
        bits: 1024,
      });

      res(result);
    });

    await db.collection(dbcol.user.name).insertOne({
      _id: login,
      password,
      email,
      isAdmin,
    });

    await db.collection(dbcol.rsa.name).insertOne({
      _id: login,
      private: rsaKeypair.private,
      public: rsaKeypair.public,
    });

    await db.collection(dbcol.jwt.name).insertOne({
      _id: login,
      jwts: [],
    });

    ctx.response.body = {
      success: true,
      description: `User ${login} successfuly registered. Log In now.`,
    };
  } catch (err) {
    respondWithError(err, ctx);
  }
});

module.exports = router;
