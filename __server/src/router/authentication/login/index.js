const Router = require('koa-router');

const jwt = require('../../../utils/jwt');
const {
  respondWithError,
  ResponseError,
} = require('../../../utils/respondWithError');

const router = new Router();

router.post('/', async ctx => {
  try {
    const {
      password,
      email,
    } = ctx.request.body;

    const {
      db,
      dbcol,
    } = ctx;

    const mFindUser = await db.collection(dbcol.user.name).findOne({
      email,
      password,
    });

    if (!mFindUser) {
      throw new ResponseError(401, 'User with such credentials does not exist');
    }

    const {
      _id: userId,
    } = mFindUser;

    const mFindRSA = await db.collection(dbcol.rsa.name).findOne({
      _id: userId,
    });

    const token = await jwt.sign(userId, mFindRSA.private);

    await db.collection(dbcol.jwt.name).updateOne(
      {
        _id: userId,
      },
      {
        $push: {
          jwts: token,
        },
      },
      {
        upsert: true,
      },
    );

    ctx.cookies.set(
      'chatter_token',
      JSON.stringify(token),
      {
        httpOnly: false,
      },
    );

    ctx.cookies.set(
      'chatter_rsaPublic',
      JSON.stringify(mFindRSA.public),
      {
        httpOnly: false,
      },
    );

    const userSent = {
      ...mFindUser,
    };
    delete userSent.password;

    ctx.response.body = {
      success: true,
      description: `User ${userId} successfully logged in`,
      data: {
        user: userSent,
      },
    };
  } catch (err) {
    respondWithError(err, ctx);
  }
});

module.exports = router;
