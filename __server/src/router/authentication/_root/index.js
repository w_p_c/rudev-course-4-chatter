const Router = require('koa-router');
const send = require('koa-send');

const router = new Router();

router
  .get('/:data*', async ctx => {
    const {
      params: {
        data,
      },
    } = ctx;

    let pathToFile = data;

    if (!data) {
      pathToFile = 'index.html';
    }

    await send(
      ctx,
      pathToFile,
      {
        root: './../_chat/build/',
      },
    );
  });

module.exports = router;
