module.exports = {
  name: 'message',
  options: {
    validator: {
      $jsonSchema: {
        bsonType: 'object',
        required: ['text', 'creatorId', 'createdAt'],
        properties: {
          text: {
            bsonType: 'string',
          },
          creatorId: {
            bsonType: 'string',
          },
          createdAt: {
            bsonType: 'date',
          },
        },
      },
    },
  },
};
