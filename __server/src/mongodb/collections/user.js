module.exports = {
  name: 'user',
  options: {
    validator: {
      $jsonSchema: {
        bsonType: 'object',
        required: ['_id', 'password', 'email', 'isAdmin'],
        properties: {
          _id: {
            bsonType: 'string',
          },
          password: {
            bsonType: 'string',
          },
          email: {
            bsonType: 'string',
          },
          isAdmin: {
            bsonType: 'bool',
          },
        },
      },
    },
  },
};
