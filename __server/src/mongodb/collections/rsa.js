module.exports = {
  name: 'rsa',
  options: {
    validator: {
      $jsonSchema: {
        bsonType: 'object',
        required: ['private', 'public', '_id'],
        properties: {
          _id: {
            bsonType: 'string',
          },
          private: {
            bsonType: 'string',
          },
          public: {
            bsonType: 'string',
          },
        },
      },
    },
  },
};
