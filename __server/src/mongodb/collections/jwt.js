module.exports = {
  name: 'jwt',
  options: {
    validator: {
      $jsonSchema: {
        bsonType: 'object',
        required: ['_id', 'jwts'],
        properties: {
          _id: {
            bsonType: 'string',
          },
          jwts: {
            bsonType: 'array',
          },
        },
      },
    },
  },
};
