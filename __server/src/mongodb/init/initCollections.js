const collections = require('../collections/index.js');

module.exports = db => {
  const promises = [];

  for (const col of Object.keys(collections)) {
    const collection = collections[col];

    promises.push(new Promise((res, rej) => {
      db.createCollection(
        collection.name,
        collection.options,
        err => {
          if (err) {
            rej(err);
          } else {
            res(db);
          }
        },
      );
    }));
  }

  return Promise.all(promises);
};
