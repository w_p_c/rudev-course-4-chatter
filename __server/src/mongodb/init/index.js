const { exec } = require('child_process');
const { promisify } = require('util');
const mongodb = require('mongodb');

const initCollections = require('./initCollections.js');

const execAsync = promisify(exec);

const {
  appConfig: {
    dev: {
      mongodb: {
        name,
        port,
        host,
        dbpath,
        logpath,
      },
    },
  },
} = global;

const getClient = () => mongodb.MongoClient(
  `mongodb://${host}:${port}`,
  {
    useNewUrlParser: true,
  },
);

module.exports = async () => {
  const shutDownDb = `sudo mongod --shutdown --dbpath=${dbpath}`;
  const upDb = `sudo mongod --fork --bind_ip ${host} --port ${port} --dbpath=${dbpath} --logpath ${logpath}`;

  const connect = client => new Promise((res, rej) => {
    client.connect(err => {
      if (!err) {
        console.log(`Successfully connected to mongodb "${name}" on port ${port}`);

        res(client);
      } else {
        console.log(`Failed to connect to mongodb "${name}" on port ${port}`);

        rej(err);
      }
    });
  });

  let shouldUpDb = false;
  let client = null;
  try {
    client = await connect(getClient());
  } catch (err) {
    if (err.name === 'MongoNetworkError') {
      shouldUpDb = true;
    } else {
      throw err;
    }
  }

  if (shouldUpDb) {
    console.log('Shutting DB down.');

    let outShutDownDb = null;
    try {
      outShutDownDb = (await execAsync(shutDownDb)).stdout;
    } catch (err) {
      outShutDownDb = err.stderr;
    }

    console.log(`DB shutdown message: "${outShutDownDb}".`);
    console.log('Setting DB up.');

    await execAsync(upDb);

    console.log('DB is up.');

    client = await connect(getClient());
  }

  console.log('Initializing DB collections.');

  await initCollections(client.db(name));

  console.log('DB collections are initialized.');

  return client;
};
