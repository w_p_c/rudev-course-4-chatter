const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const http = require('http');

/* eslint-disable import/newline-after-import */
const config = require('./../../config');
global.appConfig = config;

const router = require('./router');
const logRequest = require('./middleware/logRequest');
const mongodb = require('./mongodb');
const mongoCollections = require('./mongodb/collections');
const handleServerError = require('./utils/handleServerError.js');
const websocket = require('./websocket');
const allowCors = require('./middleware/allowCors');
const setResponseStatus = require('./middleware/setResponseStatus');

const main = async () => {
  const {
    dev: {
      server: {
        port,
      },
      mongodb: {
        name: mongodbName,
      },
    },
  } = config;

  const mongoClient = await mongodb.init();

  const app = new Koa();
  const server = http.createServer(app.callback());

  const db = mongoClient.db(mongodbName);
  const dbcol = mongoCollections;
  app.context.db = db;
  app.context.dbcol = dbcol;

  app.on('error', handleServerError);

  app
    .use(logRequest())
    .use(allowCors())
    .use(bodyParser())
    .use(setResponseStatus(200))
    .use(router.routes())
    .use(router.allowedMethods());

  server.on('upgrade', websocket(db, dbcol));

  server.listen(port);

  console.log(`\nServer started on port ${port}.`);
};

main();
