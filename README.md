## Requirements
1. Mongo 4.0.5 (Version may be lower, but project isn't tested on it)

## Setup
1. `$ cd sergey-frontend-challenge`
2. `$ yarn install`
3. `$ cd ./__server && yarn install && cd ..;`
4. `$ cd ./_chat && yarn install && cd ..;`

## Run production
1. `$ cd ./__server && yarn start;`
2. App is accesible on `localhost:49000`

## Run development
1. Run server
    1. `$ cd ./__server && yarn start;`
    2. Server app is accesible on `localhost:49000`
2. Run client
    1. `$ cd ./__chat && yarn start;`
    2. Client app is accesible on `localhost:3000`